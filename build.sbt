import com.iheart.sbtPlaySwagger.SwaggerPlugin.autoImport.{swaggerDomainNameSpaces, swaggerTarget}
lazy val root = (project in file("."))
  .enablePlugins(PlayScala, SwaggerPlugin)
  .settings(
    organization := "com.ligadigital",
    name := "auth-service",
    version := "0.0.1",

    scalaVersion := "2.12.6",

    scalacOptions ++= Seq(
      "-target:jvm-1.8",
      "-encoding", "UTF-8",
      "-unchecked",
      "-deprecation",
      "-Yno-adapted-args",
      "-Ywarn-value-discard"
    ),

    scalastyleConfig := file("project/scalastyle_config.xml"),
    scalastyleFailOnError := true,
    scalastyleFailOnWarning := false,
    //(compile in Compile) := ((compile in Compile) dependsOn scalastyle.in(Compile).toTask("")).value

    swaggerV3 := true,
    swaggerDomainNameSpaces := Seq("com.ligadigital.auth.dto"),
    swaggerTarget := baseDirectory.value / "assets",

    libraryDependencies ++= Seq(
      "com.softwaremill.macwire"    %%  "macros" % "2.3.1",
      "ch.qos.logback"              %   "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging"  %%  "scala-logging" % "3.9.0",
      "org.scalatest"               %%  "scalatest" % "3.0.5" % Test,
      "org.mockito"                 %   "mockito-core" % "2.18.0" % Test
    )
  )
