package com.ligadigital.auth

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.ligadigital.auth.controller.{AuthenticationAction, AuthorizationController}
import com.ligadigital.auth.service.{AuthorizationService, FakeAuthorizationService}
import com.softwaremill.macwire.wire
import controllers.AssetsComponents
import play.api._
import play.api.ApplicationLoader.Context
import play.api.routing.Router
import play.api.i18n.I18nComponents
import play.api.mvc.{AnyContent, BodyParser, ControllerComponents, PlayBodyParsers}
import play.filters.HttpFiltersComponents
import router.Routes

import scala.concurrent.ExecutionContext

class AuthApplicationLoader extends ApplicationLoader {
  def load(context: ApplicationLoader.Context): Application = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment)
    }

    new AppComponents(context).application
  }
}


class AppComponents(context: Context) extends BuiltInComponentsFromContext(context) with AppModule
                                                                                    with I18nComponents
                                                                                    with HttpFiltersComponents
                                                                                    with AssetsComponents {
  lazy val prefix: String = "/"
  lazy val router: Router = wire[Routes]
}

trait AppModule {
  lazy val authenticationController: AuthorizationController = wire[AuthorizationController]

  lazy val authorizationService: AuthorizationService = wire[FakeAuthorizationService]
  def authenticationAction: AuthenticationAction = wire[AuthenticationAction]

  def controllerComponents: ControllerComponents
  def playBodyParsers: PlayBodyParsers
  def defaultBodyParsers: BodyParser[AnyContent] = playBodyParsers.anyContent
  def configuration: Configuration
  implicit def executionContext: ExecutionContext
  implicit def actorSystem: ActorSystem
  implicit def materializer: Materializer
}
