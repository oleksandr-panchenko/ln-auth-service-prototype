package com.ligadigital.auth.service

import com.ligadigital.auth.dto._
import com.typesafe.scalalogging.Logger

import scala.concurrent.{ExecutionContext, Future}

trait AuthorizationService {
  def find(userId: Long, aclClassId: Long, resourceId: Option[Long], contextIds: List[Long], clientId: Option[Long], tenantId: Option[Long]): Future[List[Rights]]
  def isAuthorized(userId: Long, aclClassId: Long, resourceId: Option[Long], contextIds: List[Long], clientId: Option[Long], tenantId: Option[Long]): Future[Boolean]
}

class FakeAuthorizationService(implicit ec: ExecutionContext) extends AuthorizationService {
  private val log = Logger[FakeAuthorizationService]

  // this is just for demonstration. in real-world, app wouldn't know anything about trainings or market etc.
  private lazy val aclClassIdMapping = Map(
    "Trainings" -> 1,
    "Market" -> 2,
    "Country" -> 3,
    "Salary" -> 4
  )

  private val rightsList = List(
    Rights(id = 1, userId = 1, aclClassId = 1, objectId = Some(1001), isGlobal = false,  access = List(Create, Read),         clientId = 1, tenantId = 1),
    Rights(id = 2, userId = 1, aclClassId = 2, objectId = None,       isGlobal = true,   access = List(Read),                 clientId = 1, tenantId = 1),
    Rights(id = 3, userId = 1, aclClassId = 3, objectId = Some(3003), isGlobal = false,  access = List(Read, Update, Delete), clientId = 1, tenantId = 2),
    Rights(id = 4, userId = 2, aclClassId = 1, objectId = None,       isGlobal = true,   access = List(Create, Read, Update), clientId = 1, tenantId = 3),
    Rights(id = 5, userId = 2, aclClassId = 4, objectId = None,       isGlobal = true,   access = List(Create, Read, Update), clientId = 2, tenantId = 3),
  )

  override def find(userId: Long,
                    aclClassId: Long,
                    resourceId: Option[Long],
                    contextIds: List[Long],
                    clientId: Option[Long],
                    tenantId: Option[Long]): Future[List[Rights]] = Future {
    log.debug(s"userId=$userId, aclClassId=$aclClassId, resourceId=$resourceId, contextIds=$contextIds, clientId=$clientId, tenantId=$tenantId")
    // Searching for direct rights
    val rights = rightsList.filter { r =>
      val objectFilter = resourceId.isEmpty || resourceId == r.objectId
      val clientTenantFilter = tenantId.forall(_ == r.tenantId) && clientId.forall(_ == r.clientId)

      r.userId == userId && r.aclClassId == aclClassId && objectFilter && clientTenantFilter
    }
    log.debug(s"Direct wrights: $rights")

    // Searching for contextual rights
    val contextualRights = if (contextIds.nonEmpty) {
      rightsList.filter { r =>
        val objectFilter = resourceId.isEmpty || resourceId == r.objectId
        val clientTenantFilter = tenantId.forall(_ == r.tenantId) && clientId.forall(_ == r.clientId)

        r.userId == userId && contextIds.contains(r.aclClassId) && objectFilter && clientTenantFilter && !rights.contains(r)
      }
    } else {
      List.empty
    }
    log.debug(s"Contextual rights: $contextualRights")

    rights ++ contextualRights
  }

  override def isAuthorized(userId: Long,
                            aclClassId: Long,
                            resourceId: Option[Long],
                            contextIds: List[Long],
                            clientId: Option[Long],
                            tenantId: Option[Long]): Future[Boolean] = {
    find(userId, aclClassId, resourceId, contextIds, clientId, tenantId).map(_.nonEmpty)
  }
}
