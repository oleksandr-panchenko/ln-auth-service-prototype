package com.ligadigital.auth.controller

import com.ligadigital.auth.dto.Authorization
import com.ligadigital.auth.service.AuthorizationService
import com.typesafe.scalalogging.Logger
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

class AuthorizationController(
                               cc: ControllerComponents,
                               authenticationAction: AuthenticationAction,
                               authorizationService: AuthorizationService
                             )(implicit ec: ExecutionContext) extends AbstractController(cc) {
  private val log = Logger[AuthorizationController]

  def getPermissions() = authenticationAction.async { implicit request =>
    val aclClassId = request.getQueryString("aclClassId").get.toLong
    val resourceId = request.getQueryString("resourceId").map(_.toLong)
    val contextIds = request.queryString.get("contextId").map(_.map(_.toLong)).map(_.toList).getOrElse(List.empty)
    val clientId = request.getQueryString("clientId").map(_.toLong)
    val tenantId = request.getQueryString("tenantId").map(_.toLong)
    val userId = request.userId
    if (tenantId.isEmpty && clientId.isDefined) {
      Future.successful(BadRequest("Client ID cannot be provided without Tenant ID"))
    } else {
      authorizationService.find(userId, aclClassId, resourceId, contextIds, clientId, tenantId).map { rights =>
        log.debug(s"Permissions: $rights")
        Ok(Json.toJson(rights))
      }
    }
  }

  def isAuthorized() = authenticationAction.async { implicit request =>
    val aclClassId = request.getQueryString("aclClassId").get.toLong
    val resourceId = request.getQueryString("resourceId").map(_.toLong)
    val contextIds = request.queryString.get("contextId").map(_.map(_.toLong)).map(_.toList).getOrElse(List.empty)
    val clientId = request.getQueryString("clientId").map(_.toLong)
    val tenantId = request.getQueryString("tenantId").map(_.toLong)
    val userId = request.userId
    if (tenantId.isEmpty && clientId.isDefined) {
      Future.successful(BadRequest("Client ID cannot be provided without Tenant ID"))
    } else {
      authorizationService.isAuthorized(userId, aclClassId, resourceId, contextIds, clientId, tenantId).map { isAuthorized =>
        Ok(Json.toJson(Authorization(isAuthorized)))
      }
    }
  }
}
