package com.ligadigital.auth.controller

import play.api.mvc._
import play.api.mvc.Results.Unauthorized

import scala.concurrent.{ExecutionContext, Future}

class AuthenticatedRequest[A](val userId: Long, request: Request[A]) extends WrappedRequest[A](request)


class AuthenticationAction(val parser: BodyParser[AnyContent])(implicit ec: ExecutionContext) extends ActionBuilder[AuthenticatedRequest, AnyContent] {
  override def invokeBlock[A](request: Request[A], block: AuthenticatedRequest[A] => Future[Result]): Future[Result] = {
    request.headers.get("UserId").fold(Future.successful(Unauthorized(""))) { userIdString =>
      val wrappedRequest = new AuthenticatedRequest(userIdString.toLong, request)
      block(wrappedRequest)
    }
  }

  override protected def executionContext: ExecutionContext = ec


}