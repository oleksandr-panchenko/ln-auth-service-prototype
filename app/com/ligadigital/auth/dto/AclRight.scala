package com.ligadigital.auth.dto

sealed trait AclRight
case object Create extends AclRight
case object Read extends AclRight
case object Update extends AclRight
case object Delete extends AclRight
