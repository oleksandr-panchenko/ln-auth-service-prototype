package com.ligadigital.auth.dto

import play.api.libs.json.{Json, Writes}

case class Rights(
                   id: Long,
                   userId: Long,
                   /** Represents a class of entity, e.g. Training, Market, Country, Pin, User, Project */
                   aclClassId: Long,
                   /** Represents an id of a concrete object */
                   objectId: Option[Long],
                   /** Shows whether the rights are related to a concrete object or to the whole class of objects */
                   isGlobal: Boolean,
                   access: List[AclRight],
                   clientId: Long,
                   tenantId: Long,
                  )

object Rights {
  implicit def writes: Writes[Rights] = (o: Rights) => {
    Json.obj(
      "id" -> o.id,
      "userId" -> o.userId,
      "aclClassId" -> o.aclClassId,
      "objectId" -> o.objectId,
      "isGlobal" -> o.isGlobal,
      "access" -> o.access.map(_.toString.toUpperCase),
      "clientId" -> o.clientId,
      "tenantId" -> o.tenantId
    )
  }
}
