package com.ligadigital.auth.dto

import play.api.libs.json.{Json, Writes}

case class Authorization(
                          isAuthorized: Boolean
                        )

object Authorization {
  implicit def writes: Writes[Authorization] = Json.format[Authorization]
}
